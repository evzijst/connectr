Proof of concept for displaying smart commit information from Bitbucket repositories in JIRA issues through an Atlassian Connect app.

This mimics the functionality of the DVCS Connector, but without the need to build a complete index of the repo inside JIRA. Instead, the commits tab is a Connect app that fetches Bitbucket's Elasticsearch instance for the JIRA issue key and renders the list of commits that mention the issue in question.

As per: [https://www.youtube.com/watch?v=7-QD1kGME0k](https://www.youtube.com/watch?v=7-QD1kGME0k)