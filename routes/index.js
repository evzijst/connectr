module.exports = function (app, addon) {

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    // This is an example route that's used by the default "generalPage" module.
    // Verify that the incoming request is authenticated with Atlassian Connect
    app.get('/commits', addon.authenticate(), function (req, res) {
            var request = require('request')
            request({
                "uri": "http://localhost:9200/bitbucket-commits-*/_search?q=jira:" + req.query.issueKey,
                "json": true
            }, function(err, resp, body) {
                if(!err && resp.statusCode == 200) {
                    var commits = new Array();
                    for (var i = 0; i < body.hits.hits.length; i++) {
                        commits[i] = body.hits.hits[i]._source
                        commits[i].sha = commits[i].sha.substring(0, 7);
                    }
                    res.render('commits', {
                        commits: commits
                    });
                } else {
                    console.log("Fail");
                }
            });
        }
    );

    // Add any additional route handlers you need for views or REST resources here...

};
